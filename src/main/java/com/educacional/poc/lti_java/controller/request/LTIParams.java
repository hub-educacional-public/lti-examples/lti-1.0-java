package com.educacional.poc.lti_java.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class LTIParams {

    @JsonProperty("lti_message_type")
    private String messageType;

    @JsonProperty("lti_version")
    private String version;

    @JsonProperty("resource_link_id")
    private String resourceLinkId;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("custom_product")
    private String customProduct;

    @JsonProperty("lis_person_name_given")
    private String personNameGiven;

    @JsonProperty("lis_person_contact_email_primary")
    private String personEmail;

    @JsonProperty("lis_person_name_family")
    private String personNameFamily;

    @JsonProperty("roles")
    private String roles;

    @JsonProperty("custom_school_user_id")
    private String schoolUserId;

    @JsonProperty("custom_school_hub_id")
    private String schoolHubId;

    @JsonProperty("custom_url_redirect")
    private String urlRedirect;

    @JsonProperty("app_id")
    private String appId;

    @JsonProperty("oauth_timestamp")
    private long oauthTimestamp;

    @JsonProperty("oauth_nonce")
    private long oauthNonce;

    @JsonProperty("oauth_body_hash")
    private String oauthBodyHash;

    @JsonProperty("oauth_consumer_key")
    private String oauthConsumerKey;

    @JsonProperty("oauth_token")
    private String oauthToken;

    @JsonProperty("oauth_signature_method")
    private String oauthSignatureMethod;

    @JsonProperty("oauth_signature")
    private String oauthSignature;

    public Map<String, String> getValues() {
        Map<String, String> params = new HashMap<>();
        params.put("lti_message_type", this.messageType);
        params.put("lti_version", this.version);
        params.put("resource_link_id", this.resourceLinkId);
        params.put("user_id", this.userId);
        params.put("custom_product", this.customProduct);
        params.put("lis_person_name_given", this.personNameGiven);
        params.put("lis_person_contact_email_primary", this.personEmail);
        params.put("lis_person_name_family", this.personNameFamily);
        params.put("roles", this.roles);
        params.put("custom_school_user_id", this.schoolUserId);
        params.put("custom_school_hub_id", this.schoolHubId);
        params.put("custom_url_redirect", this.urlRedirect);
        params.put("app_id", this.appId);
        params.put("oauth_timestamp", String.valueOf(this.oauthTimestamp));
        params.put("oauth_nonce", String.valueOf(this.oauthNonce));
        params.put("oauth_body_hash", this.oauthBodyHash);
        params.put("oauth_consumer_key", this.oauthConsumerKey);
        params.put("oauth_token", this.oauthToken);
        params.put("oauth_signature_method", this.oauthSignatureMethod);
        params.put("oauth_signature", this.oauthSignature);
        return params;
    }

    public String getOauthConsumerKey() {
        return oauthConsumerKey;
    }
}
