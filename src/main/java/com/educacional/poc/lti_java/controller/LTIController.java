package com.educacional.poc.lti_java.controller;


import com.educacional.poc.lti_java.controller.request.LTIRequestBody;
import com.educacional.poc.lti_java.validator.LTIValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LTIController {

    private final LTIValidator ltiValidator;

    public LTIController(LTIValidator ltiValidator) {
        this.ltiValidator = ltiValidator;
    }

    @PostMapping(value = "/lti")
    @ResponseBody
    public String lti(@RequestBody LTIRequestBody ltiRequestBody) {
        // Validate the LTI request signature
        if (!ltiValidator.isValidSignature(ltiRequestBody)) {
            return "Invalid LTI request signature. Check the logs.";
        } else {
            // @TODO With a valid signature, the edtech must now log the user
            // into its solution and redirect them to the platform.
            return "LTI request processed successfully!";
        }
    }
}