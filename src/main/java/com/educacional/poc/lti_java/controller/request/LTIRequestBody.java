package com.educacional.poc.lti_java.controller.request;


import com.fasterxml.jackson.annotation.JsonProperty;

public class LTIRequestBody {

    @JsonProperty("action")
    private String action;

    @JsonProperty("method")
    private String method;

    @JsonProperty("params")
    private LTIParams params;

    public String getAction() {
        return action;
    }

    public String getMethod() {
        return method;
    }

    public LTIParams getParams() {
        return params;
    }

}

