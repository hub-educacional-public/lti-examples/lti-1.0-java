package com.educacional.poc.lti_java.validator;

import com.educacional.poc.lti_java.controller.request.LTIRequestBody;
import net.oauth.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;

@Component
public class LTIValidator {

    @Value("${lti.consumer_secret}")
    private String consumerSecret;

    public boolean isValidSignature(LTIRequestBody ltiRequest) {
        try {
            // Construct the OAuth message from the LTI request parameters
            OAuthMessage oauthMessage = new OAuthMessage(
                    ltiRequest.getMethod(), ltiRequest.getAction(),
                    ltiRequest.getParams().getValues().entrySet());

            // Set the OAuth consumer key and secret
            OAuthValidator validator = new SimpleOAuthValidator();
            OAuthConsumer consumer = new OAuthConsumer(
                    null, ltiRequest.getParams().getOauthConsumerKey(), consumerSecret, null);
            OAuthAccessor accessor = new OAuthAccessor(consumer);

            // validate signature
            validator.validateMessage(oauthMessage, accessor);

            // valid signature
            return true;

        } catch (IOException | OAuthException | URISyntaxException e) {
            System.out.println("Error processing LTI request: " + e);
        }

        //invalid signature
        return false;
    }

}
